package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Constants;
import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.Flowers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private String xmlFileName;
	private String currentTag;
	private Flowers flowers;
	private List<Flower> flowerList;
	private Flower flower;
	private Flower.VisualParameters visualParameters;
	private Flower.GrowingTips growingTips;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parse() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		File file = new File(xmlFileName);
		parser.parse(file, this);

		return flowers;
	}

	@Override
	public void startDocument() throws SAXException {
		flowers = new Flowers();
		flowerList = new LinkedList<>();
	}

	@Override
	public void endDocument() throws SAXException {
		flowers.setFlowerList(flowerList);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentTag = qName;
		switch (currentTag) {
			case Constants.FLOWERS:
				flowers.setRoot(qName);
				break;
			case Constants.FLOWER:
				flower = new Flower();
				flower.setRoot(qName);
				break;
			case Constants.VISUAL_PARAMETERS:
				visualParameters = new Flower.VisualParameters();
				visualParameters.setRoot(currentTag);
				break;
			case Constants.GROWING_TIPS:
				growingTips = new Flower.GrowingTips();
				growingTips.setRoot(currentTag);
				break;
			case Constants.AVE_LEN_FLOWER:
				visualParameters.setAveLenFlowerMeasure(attributes.getValue(0));
				break;
			case Constants.TEMPRETURE:
				growingTips.setTempretureMeasure(attributes.getValue(0));
				break;
			case Constants.LIGHTING:
				growingTips.setLightingLightRequiring(attributes.getValue(0));
				break;
			case Constants.WATERING:
				growingTips.setWateringMeasure(attributes.getValue(0));
				break;
			default:
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
			case Constants.FLOWER:
				flowerList.add(flower);
				break;
			case Constants.VISUAL_PARAMETERS:
				flower.setVisualParameters(visualParameters);
				break;
			case Constants.GROWING_TIPS:
				flower.setGrowingTips(growingTips);
				break;
			default:
				break;
		}
		currentTag = null;
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if(currentTag == null)
			return;
		switch (currentTag) {
			case Constants.NAME:
				flower.setName(new String(ch, start, length));
				break;
			case Constants.SOIL:
				flower.setSoil(new String(ch, start, length));
				break;
			case Constants.ORIGIN:
				flower.setOrigin(new String(ch, start, length));
				break;
			case Constants.STEM_COLOUR:
				visualParameters.setStemColour(new String(ch, start, length));
				break;
			case Constants.LEAF_COLOUR:
				visualParameters.setLeafColour(new String(ch, start, length));
				break;
			case Constants.AVE_LEN_FLOWER:
				visualParameters.setAveLenFlower(new String(ch, start, length));
				break;
			case Constants.TEMPRETURE:
				growingTips.setTempreture(new String(ch, start, length));
				break;
			case Constants.WATERING:
				growingTips.setWatering(new String(ch, start, length));
				break;
			case Constants.MULTIPLYING:
				flower.setMultiplying(new String(ch, start, length));
				break;
			default:
				break;
		}
	}

}