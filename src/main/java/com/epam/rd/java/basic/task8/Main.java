package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.epam.rd.java.basic.task8.controller.WriteOutputXml;
import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.Flowers;

import java.util.Comparator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		String xsdFileName = "input.xsd";
		System.out.println("Input ==> " + xmlFileName);
		WriteOutputXml writeOutputXml = new WriteOutputXml();

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		if (!writeOutputXml.validateXML(xsdFileName, xmlFileName))
			throw new Exception("File " + xmlFileName + " is not valid");
		Flowers flowers1 = domController.parse();
		flowers1.getFlowerList().sort(Comparator.comparing(Flower::getName));
		// sort (case 1)
		// PLACE YOUR CODE HERE

		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		writeOutputXml.createOutputXml(flowers1, outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		if (!writeOutputXml.validateXML(xsdFileName, outputXmlFile))
			throw new Exception("File " + xmlFileName + " is not valid");
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		Flowers flowers2 = saxController.parse();
		flowers2.getFlowerList().sort(Comparator.comparing(Flower::getOrigin));
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		writeOutputXml.createOutputXml(flowers2, outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		if (!writeOutputXml.validateXML(xsdFileName, outputXmlFile))
			throw new Exception("File " + xmlFileName + " is not valid");
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		Flowers flowers3 = staxController.parse();
		flowers3.getFlowerList().sort(Comparator.comparing(Flower::getSoil));
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		writeOutputXml.createOutputXml(flowers3, outputXmlFile);
		if (!writeOutputXml.validateXML(xsdFileName, outputXmlFile))
			throw new Exception("File " + xmlFileName + " is not valid");
	}

}
