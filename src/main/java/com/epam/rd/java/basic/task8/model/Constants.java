package com.epam.rd.java.basic.task8.model;

public class Constants {
    public static final String FLOWERS = "flowers";
    public static final String FLOWER = "flower";
    public static final String NAME = "name";
    public static final String SOIL = "soil";
    public static final String ORIGIN = "origin";
    public static final String VISUAL_PARAMETERS = "visualParameters";
    public static final String GROWING_TIPS = "growingTips";
    public static final String MULTIPLYING = "multiplying";
    public static final String TEMPRETURE = "tempreture";
    public static final String LIGHTING = "lighting";
    public static final String WATERING = "watering";
    public static final String MEASURE = "measure";
    public static final String LIGHT_REQUIRING = "lightRequiring";
    public static final String STEM_COLOUR = "stemColour";
    public static final String LEAF_COLOUR = "leafColour";
    public static final String AVE_LEN_FLOWER = "aveLenFlower";
    public static final String[] xmlns = {"xmlns","http://www.nure.ua"};
    public static final String[] schemaXmlns = {"xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance"};
    public static final String[] schemaLocation = {"xsi:schemaLocation", "http://www.nure.ua input.xsd"};

    private Constants(){}
}
